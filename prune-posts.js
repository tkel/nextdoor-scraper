const fs = require('fs')
const posts = require('./posts.json')

/* eslint-disable camelcase */
// comment type
function commentType(post) {
  const {
    body, photos = [], creation_time_text, edit_time_text, id,
    reaction_summaries, content_timestamp_text, subject, documents,
  } = post
  const authorNode = post.author || post.author_new
  const { name, id: authorId, from: { id: fromId, name: fromName } } = authorNode

  const summaries = reaction_summaries.summaries.map(({ count, name }) => ({ count, name }))

  // const attachments = (post.attachments || []).map(({ content, type }) => ({ content, type }))

  const result = {
    // id,
    body,
    subject,
    // documents,
    creation_time_text,
    content_timestamp_text,
    author: { name, /* id: authorId, */from: { /*id: fromId, */name: fromName } },
    reaction_summaries: summaries,
  }

  if (photos.length) result.photos = photos
  // if (attachments.length) result.attachments = attachments
  if (edit_time_text) result.edit_time_text = edit_time_text

  return result
}

function postType() {}

function feedItemHeadType() {}

const pruned = posts.map(commentType)
fs.writeFileSync('posts-pruned.json', JSON.stringify(pruned))
