
const express = require('express')
const fs = require('fs')
const cors = require('cors')
const bodyParser = require('body-parser')

const port = 3000
const app = express()

app.use(cors())
app.use(bodyParser.text())
app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded

app.post('/write', (req, res) => {
  console.log(req.body)
  fs.appendFileSync('posts.json', req.body+',\n')
  res.send('wrote to file')
})

app.post('/write/:file', (req, res) => {
  console.log(req.body)
  fs.appendFileSync(req.params.file, req.body+',\n')
  res.send('wrote to file')
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))


// https://nextdoor.com/news_feed/?post=123887426
var getPosts = () => {

  var postIds = $$('.activity-content > a').map(a => a.href).map(url => new URL(url)).map(url => url.searchParams.get('post'))

  var body = { postId: arr[0] }
  fetch('http://localhost:3000/write/postIds', {method: 'POST', body: JSON.stringify(body) }).then(r=>r.text()).then(console.log)
}

// 36 posts
// 22 public ones?
// 263 replies
// 136 public replies ?
// 52 public reply pages ?

var i = 1;
var wait = (timeout) => {
  return new Promise(resolve => {
    setTimeout(() => resolve(), timeout * i++)
  })
}

var getComments = async (postId = '37202646') => {
  // await wait(500)

  const json = await fetch(`https://nextdoor.com/web/feeds/post/${postId}/`, { "credentials": "include", "method": "GET", "mode": "cors" })
    .then(r => r.json())

  // posts has slightly more info than json.feed_items[0]
  // posts was null in one response? 
  let mainPost = []
  if (json.posts) {
    mainPost = [].concat(json.posts.filter(post => post.author_new.name === 'David Gadd'))
  } else if (json.feed_items[0].author.name === 'David Gadd') {
    mainPost = [].concat(json.feed_items[0])
  }

  const commentIds = json.feed_items[0].all_comment_ids
  if (commentIds.length === 0) return mainPost

  let requests = []
  for (let i = 0; i < commentIds.length; i += 100) {
    requests.push(commentIds.slice(i, i+100))
  }

  const comments = await Promise.all(requests.map(commentIds => 
    fetch(`https://nextdoor.com/feeds/comments/?comment_ids=${commentIds}`, { "credentials": "include", "mode": "cors" })
    .then(r => {
      if (!r.ok) throw Error()
      return r.json()
    })
    .then(json => json.comments)
    .then(comments => comments.filter(comment => comment.author.name === 'David Gadd'))
  ))

  const all = mainPost.concat(comments).flat()
  //console.log(all)
  return all
}

var getAll = async (idArr) => {
  const all = await Promise.all(idArr.map(id => getComments(id)))
    .then(arr => arr.reduce((acc, val) => acc.concat(val), []))

  //console.log(all)
  return all.flat()
}

var outside;
var result = getAll(data.merged).then(all => {
  console.log(all)
  outside = all
})
